# %% Imports
from math import cos, frexp
from typing_extensions import OrderedDict
from datasets.load import load_dataset
import torch
import numpy as np
from numpy.linalg import norm
import transformers
from transformers import PreTrainedTokenizerFast
from torch import tensor

from transformers import modeling_utils
from transformers.models.auto.modeling_auto import AutoModelForQuestionAnswering
from transformers.models.auto.tokenization_auto import AutoTokenizer
import matplotlib.pyplot as plt
from transformers.models.bert.modeling_bert import *
from transformers import BertConfig, AutoModelForQuestionAnswering
from transformers import AutoTokenizer

# %%
batch_num = 100
seq_num = 4 

# z = torch.load('output_tensor_{}.pt'.format(batch_num))[0] 
# torch.load('output_tensor_100.pt') is a tuple (z, )

# numpy_z = z[seq_num].cpu().detach().numpy()


def cos_sim(A, B):
    return np.dot(A, B)/(norm(A) * norm(B))

# sim = np.zeros([384, 384])
# for i in range(0, 384):
#     for j in range(0, 384):
#         if i != j:
#             sim[i][j] = cos_sim(numpy_z[i], numpy_z[j])


# emb = torch.load('../embedding/embedding_{}.pt'.format(batch_num))
# numpy_e = emb[seq_num].cpu().detach().numpy()

# sim_e = np.zeros([384, 384])
# for i in range(0, 384):
#     for j in range(0, 384):
#         if i != j:
#             sim_e[i][j] = cos_sim(numpy_e[i], numpy_e[j])



# x = np.arange(0,384)
# y = np.arange(0,384)

# X,Y = np.meshgrid(x,y)
# # Z = z_func(X,Y)

# fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
# ax = plt.axes(projection="3d")
    
# ax.plot_surface(X, Y, sim, cmap='rainbow')
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')

# plt.show()

# input_ids = torch.load('../input_ids/input_id_{0}.pt'.format(batch_num))
# numpy_i = input_ids[seq_num].cpu().detach().numpy()

# tmp = []
# index_err = 0
# for i in range(0, 384):
#     for j in range(0, 384):
#         if sim[i][j] >0.99:
#             try:
#                 tmp.append(map_back_to_words(i,j))
#             except indexerror:
#                 index_err += 1

# freq = {}
# for (k, v) in tmp:
#     if k not in freq.keys():
#         freq[k] = {}

#     if v not in freq[k].keys():
#         freq[k][v] = 0
    
#     freq[k][v] += 1


# %% Load Models
tokenizer = AutoTokenizer.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    use_fast=True,
    use_auth_token=True
    )
model_config = BertConfig.from_pretrained(pretrained_model_name_or_path="/home/yuan/debug_squad")

model = AutoModelForQuestionAnswering.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    config=model_config
)

model_eddit = AutoModelForQuestionAnswering.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    config=model_config
)

model_diff_2 = AutoModelForQuestionAnswering.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    config=model_config
)

from datasets import load_dataset
raw_datasets = load_dataset('squad')
column_names = raw_datasets["validation"].column_names
question_column_name = "question" if "question" in column_names else column_names[0]
context_column_name = "context" if "context" in column_names else column_names[1]
answer_column_name = "answers" if "answers" in column_names else column_names[2]
  # Validation preprocessing
def prepare_validation_features(examples):
    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples[question_column_name] = [q.lstrip() for q in examples[question_column_name]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples[question_column_name],
        examples[context_column_name],
        truncation="only_second",
        max_length=384,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding=True,
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")

    # For evaluation, we will need to convert our predictions to substrings of the context, so we keep the
    # corresponding example_id and we will store the offset mappings.
    tokenized_examples["example_id"] = []

    for i in range(len(tokenized_examples["input_ids"])):
        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)
        context_index = 1 

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        tokenized_examples["example_id"].append(examples["id"][sample_index])

        # Set to None the offset_mapping that are not part of the context so it's easy to determine if a token
        # position is part of the context or not.
        tokenized_examples["offset_mapping"][i] = [
            (o if sequence_ids[k] == context_index else None)
            for k, o in enumerate(tokenized_examples["offset_mapping"][i])
        ]

    return tokenized_examples

# %%
eval_example = raw_datasets['validation']
eval_dataset = eval_example.map(
    prepare_validation_features,
    batched=True,
    remove_columns=column_names,
    load_from_cache_file=False,
    desc='asd'
).remove_columns(['offset_mapping', 'example_id']).select(range(1))
from torch.utils.data import SequentialSampler, DataLoader
eval_sampler = SequentialSampler(eval_dataset)
eval_dataloader = DataLoader(eval_dataset, sampler=eval_sampler)

inputs = {
       "input_ids": torch.as_tensor(eval_dataset['input_ids']),
       "attention_mask": torch.as_tensor(eval_dataset['attention_mask']),
       "token_type_ids": torch.as_tensor(eval_dataset['token_type_ids'])
        }

seq_words = tokenizer.convert_ids_to_tokens(eval_dataset['input_ids'][0])
# s = ' '.join(seq_words) 
model(**inputs)


# %% 
sim_embedding_different_layers = [] 
model_intermediate = model.bert.encoder
for layer_num in range(12):
    layer0_embedding = model.bert.encoder.layer[layer_num].attention.self.cache[0][0].cpu().detach().numpy()
    
    sim_embedding_different_layers.append(np.zeros([384, 384]))
    for i in range(0, 384):
        for j in range(0, 384):
            if i != j:
                sim_embedding_different_layers[layer_num][i][j] = cos_sim(layer0_embedding[i], layer0_embedding[j])

# %%
x = np.arange(0,170)
y = np.arange(0,170)

X,Y = np.meshgrid(x,y)
# Z = z_func(X,Y)
for i in range(0, 12):
    fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
    ax = plt.axes(projection="3d")
    ax.plot_surface(X, Y, sim_embedding_different_layers[i][0:170, 0:170], cmap='rainbow')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()


# %%

word_1_index = 18
word_2_index = 40
x = np.arange(0,12)
y = []
for i in range(0,12):
    y.append(sim_embedding_different_layers[i][word_1_index][word_2_index])

plt.plot(x,y)
plt.xlabel("Layers")
plt.ylabel("Cos similarity")
plt.title("Cos similarity Between 'american' and 'american' in different place")
plt.show()

# %%

eddited_eval_dataset = eval_dataset['input_ids']
eddited_eval_dataset[0][18] = 2329 # 2329 => british 
inputs_eddit = {
       "input_ids": torch.as_tensor(eddited_eval_dataset),
       "attention_mask": torch.as_tensor(eval_dataset['attention_mask']),
       "token_type_ids": torch.as_tensor(eval_dataset['token_type_ids'])
        }

seq_eddit_words = tokenizer.convert_ids_to_tokens(eddited_eval_dataset[0])

# s = ' '.join(seq_words) 

model_eddit(**inputs_eddit)
model_eddit_intermediate = model_eddit.bert.encoder
# model_eddit_intermediate.layer[11].attention.self.cache[0][0].cpu().detach().numpy()[18]
# model_intermediate.layer[11].attention.self.cache[0][0].cpu().detach().numpy()[18]

# %% Distence between origin and eddited seq
sim_within_single_seq = []
for layer_num in range(0, 12):
    tmp = np.zeros([384, 384])
    for i in range(0, 384):
        for j in range(0, 384):
            if i != j:
                tmp1 = model.bert.encoder.layer[layer_num].attention.self.cache[0][0].cpu().detach().numpy()[i]
                tmp2 = model.bert.encoder.layer[layer_num].attention.self.cache[0][0].cpu().detach().numpy()[j]
                tmp[i][j] = cos_sim(tmp1, tmp2)
    sim_within_single_seq.append(tmp)


x = np.arange(0,384)
y = np.arange(0,384)

X,Y = np.meshgrid(x,y)
# Z = z_func(X,Y)
for i in range(0, 12):
    fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
    ax = plt.axes(projection="3d")
    ax.plot_surface(X, Y, sim_within_single_seq[i], cmap='rainbow')
    ax.set_xlabel('x')
    ax.set_title('The cos simularity in {} layer output with in the same sequence'.format(i))
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()
    # plt.savefig("/home/yuan/10_27/part1_with_padding/{}.png".format(i))

# %% IF we do not want to see the padding's data


x = np.arange(0,171)
y = np.arange(0,171)

X,Y = np.meshgrid(x,y)
# Z = z_func(X,Y)
for i in range(0, 12):
    fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
    ax = plt.axes(projection="3d")
    ax.plot_surface(X, Y, sim_within_single_seq[i][0:171,0:171], cmap='rainbow')
    ax.set_xlabel('x')
    ax.set_title('The cos simularity in {} layer output with in the same sequence but without padding'.format(i))
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()
    # plt.savefig("/home/yuan/10_27/part1_without_padding/{}.png".format(i))

# %% Calculate the stat numbers
threshold = 0.9
for i in range(0, 12):
    print("in the {} layer {:.2f} % of the output pairs have cos similarity larger than {}".format(i, (sim_within_single_seq[i] > threshold).sum() / sim_within_single_seq[i].size * 100, threshold))

for i in range(0, 12):
    print("in the {} layer {:.2f} % of the output pairs have cos similarity larger than {}".format(i, (sim_within_single_seq[i][:171,:171] > threshold).sum() / sim_within_single_seq[i][:171,:171].size * 100, threshold))

# %% Map those back

tokenizer = PreTrainedTokenizerFast(tokenizer_file="/home/yuan/debug_squad/tokenizer.json")
words = tokenizer.convert_ids_to_tokens(list(inputs['input_ids'][0].cpu().detach().numpy()))

def map_back_to_words(i, j):
    word_1 = words[i]
    word_2 = words[j]
    return (word_1, word_2)

tmp = []
index_err = 0
for i in range(0, 384):
    for j in range(0, 384):
        if sim_within_single_seq[11][i][j] >0.9:
            try:
                tmp.append(map_back_to_words(i,j))
            except indexerror:
                index_err += 1

freq = {}
for (k, v) in tmp:
    if k not in freq.keys():
        freq[k] = {}

    if v not in freq[k].keys():
        freq[k][v] = 0
    
    freq[k][v] += 1

# %% Calculate the Cos similarity between Eddited and origin seq
sim_between_origin_and_eddit = []
for layer_num in range(0, 12):
    tmp = np.zeros([384,384])
    for i in range(0,384):
        for j in range(0, 384):
            if i != j:
                eddit = model_eddit_intermediate.layer[layer_num].attention.self.cache[0][0].cpu().detach().numpy()[i]
                origin = model_intermediate.layer[layer_num].attention.self.cache[0][0].cpu().detach().numpy()[j]
                tmp[i][j] = cos_sim(eddit, origin)

    sim_between_origin_and_eddit.append(tmp)
# %% Show the Cos similarity

x = np.arange(0,384)
y = np.arange(0,384)

X,Y = np.meshgrid(x,y)
# Z = z_func(X,Y)
for i in range(0, 12):
    fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
    ax = plt.axes(projection="3d")
    ax.plot_surface(X, Y, sim_between_origin_and_eddit[i], cmap='rainbow')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

# %% If we do not want to see the paddings

x = np.arange(0,171)
y = np.arange(0,171)

X,Y = np.meshgrid(x,y)
# Z = z_func(X,Y)
for i in range(0, 12):
    fig = plt.figure(figsize=(800/96, 800/96), dpi=96)
    ax = plt.axes(projection="3d")
    ax.plot_surface(X, Y, sim_between_origin_and_eddit[i][0:171,0:171], cmap='rainbow')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

# %%
for i in range(0, 12):
    print((sim_between_origin_and_eddit[i][0:171,0:171] > 0.6).sum() / sim_between_origin_and_eddit[i][0:171,0:171].size)

# %% measure the percentag of tow totally diff seq

eval_dataset_diff_2 = eval_example.map(
    prepare_validation_features,
    batched=True,
    remove_columns=column_names,
    load_from_cache_file=False,
    desc='asd'
).remove_columns(['offset_mapping', 'example_id']).select(range(3,4))
from torch.utils.data import SequentialSampler, DataLoader
eval_sampler = SequentialSampler(eval_dataset)

inputs_diff_2 = {
       "input_ids": torch.as_tensor(eval_dataset['input_ids']),
       "attention_mask": torch.as_tensor(eval_dataset['attention_mask']),
       "token_type_ids": torch.as_tensor(eval_dataset['token_type_ids'])
        }

seq_words_diff_2 = tokenizer.convert_ids_to_tokens(eval_dataset['input_ids'][0])
# s = ' '.join(seq_words) 
model_diff_2(**inputs_diff_2)


# %% 
# measure how many paddings in the datasets

