# %% imports
import pickle
from itertools import groupby
# %%  Read data back
with open("/home/yuan/token_relevance/0_reduced_map.pickle", 'rb') as w:
    dict_0 = pickle.load(w)
with open("/home/yuan/token_relevance/1_reduced_map.pickle", 'rb') as w:
    dict_1 = pickle.load(w)
with open("/home/yuan/token_relevance/2_reduced_map.pickle", 'rb') as w:
    dict_2 = pickle.load(w)
# %% 
#! Input, a (batch of) sequence. 
#! Output, which of them are important to each other? or The other choice, eleminate
#! those that are not important to any one? or something in between, for each token, we ask the
#! rest of them see if there's any one interest, we can accumulate the "intersts score" for the token,
#! if the score is higher than a certain threshold or rank them only peak the highest severals.
from math import cos, frexp
from typing_extensions import OrderedDict
from datasets.load import load_dataset, load_metric
import torch
import numpy as np
from numpy.linalg import norm
import transformers
from transformers import PreTrainedTokenizerFast
from torch import tensor

from transformers import modeling_utils
from transformers.models.auto.modeling_auto import AutoModelForQuestionAnswering
from transformers.models.auto.tokenization_auto import AutoTokenizer
import matplotlib.pyplot as plt
from transformers.models.bert.modeling_bert import *
from transformers import BertConfig, AutoModelForQuestionAnswering
from transformers import AutoTokenizer
from transformers.trainer_utils import EvalPrediction

def cos_sim(A, B):
    return np.dot(A, B)/(norm(A) * norm(B))

# %% Load The Model and its utils 

tokenizer = AutoTokenizer.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    use_fast=True,
    use_auth_token=True
    )
model_config = BertConfig.from_pretrained(pretrained_model_name_or_path="/home/yuan/debug_squad")

model = AutoModelForQuestionAnswering.from_pretrained(
    pretrained_model_name_or_path="/home/yuan/debug_squad",
    config=model_config
)
# %%
from datasets import load_dataset
raw_datasets = load_dataset('squad')
column_names = raw_datasets["train"].column_names
question_column_name = "question" if "question" in column_names else column_names[0]
context_column_name = "context" if "context" in column_names else column_names[1]
answer_column_name = "answers" if "answers" in column_names else column_names[2]

# Training preprocessing
def prepare_train_features(examples):
    pad_on_right = 1
    max_seq_length = 384

    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples[question_column_name] = [q.lstrip() for q in examples[question_column_name]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples[question_column_name if pad_on_right else context_column_name],
        examples[context_column_name if pad_on_right else question_column_name],
        truncation="only_second" if pad_on_right else "only_first",
        max_length=max_seq_length,
        stride=128,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")
    # The offset mappings will give us a map from token to character position in the original context. This will
    # help us compute the start_positions and end_positions.
    offset_mapping = tokenized_examples.pop("offset_mapping")

    # Let's label those examples!
    tokenized_examples["start_positions"] = []
    tokenized_examples["end_positions"] = []

    for i, offsets in enumerate(offset_mapping):
        # We will label impossible answers with the index of the CLS token.
        input_ids = tokenized_examples["input_ids"][i]
        cls_index = input_ids.index(tokenizer.cls_token_id)

        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        answers = examples[answer_column_name][sample_index]
        # If no answers are given, set the cls_index as answer.
        if len(answers["answer_start"]) == 0:
            tokenized_examples["start_positions"].append(cls_index)
            tokenized_examples["end_positions"].append(cls_index)
        else:
            # Start/end character index of the answer in the text.
            start_char = answers["answer_start"][0]
            end_char = start_char + len(answers["text"][0])

            # Start token index of the current span in the text.
            token_start_index = 0
            while sequence_ids[token_start_index] != (1 if pad_on_right else 0):
                token_start_index += 1

            # End token index of the current span in the text.
            token_end_index = len(input_ids) - 1
            while sequence_ids[token_end_index] != (1 if pad_on_right else 0):
                token_end_index -= 1

            # Detect if the answer is out of the span (in which case this feature is labeled with the CLS index).
            if not (offsets[token_start_index][0] <= start_char and offsets[token_end_index][1] >= end_char):
                tokenized_examples["start_positions"].append(cls_index)
                tokenized_examples["end_positions"].append(cls_index)
            else:
                # Otherwise move the token_start_index and token_end_index to the two ends of the answer.
                # Note: we could go after the last offset if the answer is the last word (edge case).
                while token_start_index < len(offsets) and offsets[token_start_index][0] <= start_char:
                    token_start_index += 1
                tokenized_examples["start_positions"].append(token_start_index - 1)
                while offsets[token_end_index][1] >= end_char:
                    token_end_index -= 1
                tokenized_examples["end_positions"].append(token_end_index + 1)

    return tokenized_examples

    # Here I define the Max train samples to limited the preprocessing time
max_train_samples = 1
if 1:
    if "train" not in raw_datasets:
        raise ValueError("--do_train requires a train dataset")
    train_dataset = raw_datasets["train"]
        # We will select sample from whole data if agument is specified
    train_dataset = train_dataset.select(range(max_train_samples))
    # Create train feature from dataset
    train_dataset = train_dataset.map(
            prepare_train_features,
            batched=True,
            num_proc=1,
            remove_columns=column_names,
            load_from_cache_file=False,
            desc="Running tokenizer on train dataset",
    )
        # Number of samples might increase during Feature Creation, We select only specified max samples
    # train_dataset = train_dataset.select(range(max_train_samples))   # Training preprocessing

# %% 
#! Validation preprocessing
raw_datasets = load_dataset('squad')
column_names = raw_datasets["validation"].column_names
question_column_name = "question" if "question" in column_names else column_names[0]
context_column_name = "context" if "context" in column_names else column_names[1]
answer_column_name = "answers" if "answers" in column_names else column_names[2]

def prepare_validation_features(examples):
    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples[question_column_name] = [q.lstrip() for q in examples[question_column_name]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples[question_column_name],
        examples[context_column_name],
        truncation="only_second",
        max_length=384,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding=True,
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")

    # For evaluation, we will need to convert our predictions to substrings of the context, so we keep the
    # corresponding example_id and we will store the offset mappings.
    tokenized_examples["example_id"] = []

    for i in range(len(tokenized_examples["input_ids"])):
        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)
        context_index = 1 

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        tokenized_examples["example_id"].append(examples["id"][sample_index])

        # Set to None the offset_mapping that are not part of the context so it's easy to determine if a token
        # position is part of the context or not.
        tokenized_examples["offset_mapping"][i] = [
            (o if sequence_ids[k] == context_index else None)
            for k, o in enumerate(tokenized_examples["offset_mapping"][i])
        ]

    return tokenized_examples

# %% Load Validation Dataset 
seq_index = 21
eval_example = raw_datasets['validation']
eval_dataset = eval_example.map(
    prepare_validation_features,
    batched=True,
    remove_columns=column_names,
    load_from_cache_file=False,
    desc='asd'
).remove_columns(['offset_mapping', 'example_id']).select(range(seq_index-1, seq_index))
from torch.utils.data import SequentialSampler, DataLoader, dataloader
eval_sampler = SequentialSampler(eval_dataset)
eval_dataloader = DataLoader(eval_dataset, sampler=eval_sampler)

# %%  Setting up the mask
accumulate_attention_score_theshold = -100
relevance_mask = np.full((12,12,384,384), -np.inf) #! [Layer_num, head_num, token, token]
for layer in range(12):
    for head in range(12):
        for iindex, itoken in enumerate(eval_dataset['input_ids'][0]):
            for jindex, jtoken in enumerate(eval_dataset['input_ids'][0]):
                if jtoken in dict_2[layer][itoken][head].keys() and dict_2[layer][itoken][head][jtoken][0] > accumulate_attention_score_theshold:
                    # if this two words are relevance
                    relevance_mask[layer][head][iindex][jindex] = 0 
                elif jtoken == 102 or jtoken == 101:
                    relevance_mask[layer][head][iindex][jindex] = 0
                else:
                    # if these two words are not relevance
                    pass

# relevance_mask = np.zeros((12,12,384,384))
# relevance_mask[:, :, :, 171:] += -np.inf
 

# %%
inputs = {
       "input_ids": torch.as_tensor(eval_dataset['input_ids']),
       "attention_mask": torch.as_tensor(eval_dataset['attention_mask']),
       "token_type_ids": torch.as_tensor(eval_dataset['token_type_ids']),
       "relevance_mask": torch.as_tensor(relevance_mask.astype(np.float32))
        }

seq_words = tokenizer.convert_ids_to_tokens(eval_dataset[0]['input_ids'])


# %% Load Train Dataset
# #! dict[layer_id][token_id][head_id] = tuple(token_id, attention_score, attention_prob)
# #! output dict[layer_id][token_id][head_id] = tuple(token_id, \sigma{attention_score}, \sigma{attention_prob} )
# from torch.utils.data import SequentialSampler, DataLoader 
# train_sample = SequentialSampler(train_dataset)
# train_dataloader = DataLoader(train_dataset, sampler=train_sample)

# inputs = {
#     "input_ids": torch.as_tensor(train_dataset['input_ids']),
#     "attention_mask": torch.as_tensor(train_dataset['attention_mask']),
#     "token_type_ids": torch.as_tensor(train_dataset['token_type_ids']),
#     "start_positions": torch.as_tensor(train_dataset['start_positions']),
#     "end_positions" : torch.as_tensor(train_dataset['end_positions']),
# }

# ## !Here we define a mask for each tokens, if the coorespinding token are high-relevant, then we put the mask as True
# ## !else we put hte mask as false, if such a way I can check whether the accuracy drops significantly or not.
# seq_words = tokenizer.convert_ids_to_tokens(train_dataset[0]['input_ids'])

# %% Feed the data
outputs = model(**inputs)

# # %% Apply Post-processing
# import utils_qa

# def post_processing_function(examples, features, predictions, stage="eval"):
#     # Post-processing: we match the start logits and end logits to answers in the original context.
#     predictions = utils_qa.postprocess_qa_predictions(
#         examples=examples,
#         features=features,
#         predictions=predictions,
#         # version_2_with_negative=data_args.version_2_with_negative,
#         # n_best_size=data_args.n_best_size,
#         # max_answer_length=data_args.max_answer_length,
#         # null_score_diff_threshold=data_args.null_score_diff_threshold,
#         output_dir="~/.tmp/post-processing/",
#         # log_level=log_level,
#         prefix=stage,
#     )
#     # Format the result to the format the metric expects.
#     # if data_args.version_2_with_negative:
#     #     formatted_predictions = [
#     #         {"id": k, "prediction_text": v, "no_answer_probability": 0.0} for k, v in predictions.items()
#     #     ]
#     # else:
#     formatted_predictions = [{"id": k, "prediction_text": v} for k, v in predictions.items()]

#     references = [{"id": ex["id"], "answers": ex[answer_column_name]} for ex in examples]
#     return EvalPrediction(predictions=formatted_predictions, label_ids=references)

# metric = load_metric("squad")

# def compute_metrics(p: EvalPrediction):
#     return metric.compute(preditions=p.predictions, reference=p.label_ids)

# %% See the answer

answer_start = torch.argmax(outputs[0])  # get the most likely beginning of answer with the argmax of the score
answer_end = torch.argmax(outputs[1]) + 1
answer = tokenizer.convert_tokens_to_string(tokenizer.convert_ids_to_tokens(inputs['input_ids'][0][answer_start:answer_end]))
print(answer,"\n", eval_example[seq_index-1])

# %% Calculate pruned percent

print(1 - (relevance_mask > -1).sum() / (12 * 12 * 384 * 384))

# %%
