import torch
from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt

model_init = torch.load('model_30.pt')
model_last = torch.load('model_14730.pt')



target ='bert.encoder.layer.1.attention.self.key.weight' 

init_tensor = model_init[target]
last_tensor = model_last[target]

def z_func(x, y):
    (last_tensor[x][y] - init_tensor[x][y]) / last_tensor[x][y]

x = np.linspace(0,769)
y = np.linspace(0,769)

X,Y = np.meshgrid(x,y)
Z = z_func(X,Y)

fig = plt.figure()
ax = plt.axes(projection="3d")
    
ax.plot_wireframe(X, Y, Z, color='green')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

plt.show()

