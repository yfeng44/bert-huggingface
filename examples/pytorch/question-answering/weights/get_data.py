import torch
import csv 


for i in range(1, 487):
    model1 = torch.load('model_{0}.pt'.format(i * 30))
    model2 = torch.load('model_{0}.pt'.format(i * 30 + 120))
    
    for layer in range (0, 12):
        for name in 'key', 'query', 'value':
            a = torch.abs(model1['bert.encoder.layer.{0}.attention.self.{1}.weight'.format(layer, name)] - model2['bert.encoder.layer.{0}.attention.self.{1}.weight'.format(layer, name)]) / model1['bert.encoder.layer.{0}.attention.self.{1}.weight'.format(layer, name)]
            mask01 = a <= 0.1
            mask005 = a <= 0.05
            mask001 = a <= 0.01
            
            with open('plot_data_2/layer_{}_{}_weight_over_iteration_30_lessthan_01.csv'.format(layer, name), 'a') as w:
                writer = csv.writer(w)
                writer.writerow([i, torch.count_nonzero(mask01).item() / 589824 ])

            with open('plot_data_2/layer_{}_{}_weight_over_iteration_30_lessthan_005.csv'.format(layer, name), 'a') as w:
                writer = csv.writer(w)
                writer.writerow([i, torch.count_nonzero(mask005).item() / 589824] )

            with open('plot_data_2/layer_{}_{}_weight_over_iteration_30_lessthan_001.csv'.format(layer, name), 'a') as w:
                writer = csv.writer(w)
                writer.writerow([i, torch.count_nonzero(mask001).item() / 589824])