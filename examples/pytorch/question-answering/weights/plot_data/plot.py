import csv 
import matplotlib.pyplot as plt

# key
less01 = []
less005 = []
less001 = []

x = list(range(0, 490))

for i in range(0, 12):
    less01.append([])
    with open('layer_{0}_value_weight_over_iteration_30_lessthan_001.csv'.format(i), 'r') as w:
        lines = csv.reader(w, delimiter=',')
        for row in lines:
            less01[i].append(float(row[1]))

for i in range (0, 12):
    plt.plot(x, less01[i], label = "Bert Layer {}".format(i))

plt.legend()
plt.title('How much percentage of the weights in Value(W_v) change less than 1%')
plt.xlabel("Every 30 iterations")
plt.ylabel("The weight change Percentage")
plt.show()
